from subprocess import call, PIPE, run
from time import time
import os


def get_params(path):
    params = {
        "B": None,
        "N": None,
        "k": None,
        "K": None
    }
    with open(file=path, mode='r') as _params:
        line_params = _params.readline().strip().split(";")
        params["B"] = int(line_params[0])
        params["N"] = int(line_params[1])
        params["k"] = int(line_params[2])
        params["K"] = int(line_params[3])
        params["b"] = int(line_params[4])
    return params


input_place = "Inputs/12/1"

params = get_params(os.path.join(input_place, "params.txt"))
K = params["K"]

result = run(["python3", "reduction_v2.py", "-f", os.path.join(input_place, "inputs.txt"),
              '-p', os.path.join(input_place, "params.txt")], stdout=PIPE)
output = result.stdout.decode("utf-8")
real_maxflow_reduction = int(output.split("\n")[1].split(" ")[2])
print("Graph reduction is done!")
print("Maxflow should be %d" % real_maxflow_reduction)

start = time()
result = run(["./bs/build/bikeshare", os.path.join(input_place, "edges.txt"), os.path.join(input_place, "leftover.txt"),
              os.path.join(input_place, "transport.txt")], stdout=PIPE)
output = result.stdout.decode("utf-8")
real_maxflow_cycle = int(output.split("\n")[0].split(" ")[2])
real_cost_cycle = int(output.split("\n")[1].split(" ")[2])
end = time()
print("Solving the problem is done!")
print("Solving the problem takes %f seconds" % (end-start))
print("Real cost: %d" % real_cost_cycle)
print("Maxflow is %d\n" % real_maxflow_cycle)

if real_maxflow_cycle == real_maxflow_reduction:
    pass
else:
    print("Maxflow is not correct!")

part_cost = 0
call(["python3", "partition.py", "-f", os.path.join(input_place, "inputs.txt"),
      "-p", os.path.join(input_place, "params.txt")])
print("Partition is done!")

start = time()
for i in range(K):
    result = run(
        ["python3", "reduction_v2.py", "-f", os.path.join(input_place, "partitions", "inputs_%d.txt" % (i + 1)),
         '-p', os.path.join(input_place, "params.txt"),
         '-l', os.path.join(input_place, "partitions", "leftover.txt")], stdout=PIPE)
    output = result.stdout.decode("utf-8")
    maxflow_part_reduction = int(output.split("\n")[1].split(" ")[2])

    result = run(["./bs/build/bikeshare", os.path.join(input_place, "partitions", "edges.txt"),
                  os.path.join(input_place, "partitions", "leftover.txt"),
                  os.path.join(input_place, "partitions", "transport.txt")], stdout=PIPE)
    output = result.stdout.decode("utf-8")
    maxflow_part_cycle = int(output.split("\n")[0].split(" ")[2])
    part_cost += int(output.split("\n")[1].split(" ")[2])

    if maxflow_part_reduction == maxflow_part_cycle:
        pass
    else:
        print("Maxflow is not correct at %d" % (i + 1))
    print("Maxflow for %d ith partition is done!" % (i + 1))
end = time()
print("Solving partition problem takes %f seconds" % (end-start))

print("Whole algorithm is done!\n")
print("Real cost: %d" % real_cost_cycle)
print("Total cost from partitions: %d" % part_cost)
