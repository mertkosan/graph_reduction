# File formats

## Input Format

## params.txt
B;N;k;K;b

There will be only 1 line for parameters. B is the number of bikes, N is the number of bike slots, k is the number of time slots per day, K is the number of
days, b is the cost for transporting one bike to other slot.

## inputs.txt

s1,d1;s2,d2;...;sn,dn

...

Each line is one time slot and it has supply and demand amount for each bike slots.

File contains K*k lines.

Remember total amount of supply at time=t+1 should be equal to total amount of demand at t=t

Remember total amount of supply at time=1 should be equal to total number of bikes.

## Output Format

### nodes.txt
super_source_id;demand_amount

super_sink_id;demand_amount

id1;demand_amount

...


Note that first two nodes are source (s) and sink (t) nodes, so flow will be calculated from s to t

If demand_amount > 0, it means node is demand node, < 0, it means node is supply node, if =0, it means node is
neither supply or demand node.

### edges.txt

For last leftover edges

nodei;nodej;capacity;price;l;node_number;e;e

For transport edges

nodei;nodej;capacity;price;t;from_node;to_node;time

For other edges

nodei;nodej;capacity;price;e;e;e;e

...

Each line is an edge from node i to node j with capacity and price. If capacity is -1, it means infinity.

### Versions

version 1 is 3 node model

version 2 is 1 node model

### Partition.py

Usage: python3 partition.py -f path/to/input.txt -p path/to/params.txt

It creates a new folder called "partitions" and put all partitions in that file in format "input_{i}.txt" 
starting from 1 to n.

### Reduction_v2.py

For whole graph:

Usage:  python3 reduction_v2.py -f path/to/inputs.txt -p path/to/params.txt

Note that edges.txt and nodes.txt are outputting in given input folder (not specifically output folder)

For partition method:

Usage:  python3 reduction_v2.py -f path/to/inputs_1.txt -p path/to/params.txt -l path/to/leftover.txt





