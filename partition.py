import argparse
import ntpath
import os

parser = argparse.ArgumentParser()

# Usage python3 partition.py -f path/to/input.txt -p path/to/params.txt
parser.add_argument("-f", "--file", help="File name")
parser.add_argument("-p", "--params", help="Parameters")
parser.add_argument("-l", "--leftover", help="Leftover indicator")


def get_params(path):
    params = {
        "B": None,
        "N": None,
        "k": None,
        "K": None
    }
    with open(file=path, mode='r') as _params:
        line_params = _params.readline().strip().split(";")
        params["B"] = int(line_params[0])
        params["N"] = int(line_params[1])
        params["k"] = int(line_params[2])
        params["K"] = int(line_params[3])
        params["b"] = int(line_params[4])
    return params


def divide_input(input, output_folder, params):
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    input_file_name = ntpath.basename(os.path.splitext(input)[0])
    with open(input, "r") as input_:
        for i in range(params["K"]):
            output_file_name = os.path.join(output_folder, input_file_name + "_" + str(i + 1) + ".txt")
            with open(output_file_name, "w") as output_:
                for j in range(params["k"]):
                    output_.write(input_.readline())


if __name__ == '__main__':
    args = parser.parse_args()

    input_file = args.file
    param_file = args.params

    params = get_params(param_file)

    parent = os.path.abspath(os.path.join(input_file, os.pardir))
    output_partitions_folder = os.path.join(parent, "partitions")
    divide_input(input_file, output_partitions_folder, params)

    with open(os.path.join(output_partitions_folder, "leftover.txt"), 'w') as _left:
        pass
