import os
import argparse
import ntpath

parser = argparse.ArgumentParser()

# Usage python3 partition.py -f path/to/input.txt -p path/to/params.txt
parser.add_argument("-f", "--file", help="File name")
parser.add_argument("-p", "--params", help="Parameters")
parser.add_argument("-l", "--leftover", help="Leftover Bikes")


def get_params(path):
    params = {
        "B": None,
        "N": None,
        "k": None,
        "K": None
    }
    with open(file=path, mode='r') as _params:
        line_params = _params.readline().strip().split(";")
        params["B"] = int(line_params[0])
        params["N"] = int(line_params[1])
        params["k"] = int(line_params[2])
        params["K"] = int(line_params[3])
        params["b"] = int(line_params[4])
    return params


def create_matrix_from_input(input_file, params, leftover):
    if leftover is None or os.stat(leftover).st_size == 0:
        leftover_bikes = [0 for _ in range(params["N"])]
    else:
        with open(leftover, "r") as _leftover:
            line = _leftover.readline().strip()
            leftover_bikes = line.split(";")
    matrix = []
    with open(file=input_file, mode='r') as _file:
        for line in _file:
            matrix.append([(int(i.split(",")[0].strip()), int(i.split(",")[1].strip())) for i in line.split(";")])
    for i in range(params["N"]):
        s = matrix[0][i][0]
        d = matrix[0][i][1]
        matrix[0][i] = (s + int(leftover_bikes[i]), d)
    return matrix


def construct_the_graph(matrix, params):
    """
    every node has 1 node which is only bike slot after supply and demand nodes are removed.
    """
    id = 4
    graph = {
        "source_node": (1, 0),
        "sink_node": (2, 0),
        "sink_2_node": (3, 0),
        "nodes": [],
        "edges": []
    }
    # set sink2_node demand
    first_layer_supply = sum([pair[0] for pair in matrix[0]])
    last_layer_demand = sum([pair[1] for pair in matrix[-1]])

    if last_layer_demand > first_layer_supply:
        print("ERROR, last_layer_demand cannot be bigger than first_layer_supply")

    graph["sink_2_node"] = (3, first_layer_supply - last_layer_demand)

    # central nodes
    for i in range(params["k"] * params["K"]):
        for j in range(params["N"]):
            graph["nodes"].append((id, matrix[i][j][1] - matrix[i][j][0]))
            id += 1

    # create edges
    graph["edges"].append((graph["sink_2_node"][0], graph["sink_node"][0], abs(graph["sink_2_node"][1]), 0,
                           'e', 'e', 'e', 'e'))

    # from main source and to main sink
    for k in range(len(graph["nodes"])):
        if graph["nodes"][k][1] < 0:
            graph["edges"].append((graph["source_node"][0], graph["nodes"][k][0], abs(graph["nodes"][k][1]), 0,
                                   'e', 'e', 'e', 'e'))
        elif graph["nodes"][k][1] > 0:
            graph["edges"].append((graph["nodes"][k][0], graph["sink_node"][0], abs(graph["nodes"][k][1]), 0,
                                   'e', 'e', 'e', 'e'))

    # from last bike slot nodes to sink2
    for k in range(1, params["N"] + 1):
        graph["edges"].append((graph["nodes"][-k][0], graph["sink_2_node"][0], -1, 0, 'l', params["N"]-k+1, 'e', 'e'))

    # leftover bikes edges
    for k in range(0, len(graph["nodes"]) - params["N"] - 1, params["N"]):
        for i in range(params["N"]):
            graph["edges"].append((graph["nodes"][k + i][0], graph["nodes"][k + params["N"] + i][0], -1, 0,
                                   'e', 'e', 'e', 'e'))

    # transport bikes edges
    times = [int((time * params["k"]) / 2 + 1) for time in range(2 * params["K"])]
    for time in times:
        nodes = [graph["nodes"][(time - 1) * params["N"] + i][0] for i in range(params["N"])]
        for i, node_i in enumerate(nodes):
            for j, node_j in enumerate(nodes):
                if i != j:
                    graph["edges"].append((node_i, node_j, -1, params["b"], 't', i+1, j+1, time))

    return graph


def verify_graph(graph, version):
    total_demand = 0
    for node in graph["nodes"]:
        total_demand += node[1]
    total_demand += graph["sink_2_node"][1]
    if version == 1:
        total_demand += graph["source_2_node"][1]
    if total_demand != 0:
        return False
    else:
        return True


def create_output(graph, version, output):
    """
    take graph as an input, create a text file which has a name: output_name
    """
    if not os.path.exists(output):
        os.makedirs(output)

    with open(output + "/nodes.txt", "w") as _nodes:
        _nodes.write("%d;%d\n" % (graph["source_node"][0], graph["source_node"][1]))
        _nodes.write("%d;%d\n" % (graph["sink_node"][0], graph["sink_node"][1]))
        if version == 1:
            _nodes.write("%d;%d\n" % (graph["source_2_node"][0], graph["source_2_node"][1]))
        _nodes.write("%d;%d\n" % (graph["sink_2_node"][0], graph["sink_2_node"][1]))
        for node in graph["nodes"]:
            _nodes.write("%d;%d\n" % (node[0], node[1]))

    with open(output + "/edges.txt", "w") as _edges:
        maxflow = 0
        for edge in graph["edges"]:
            if edge[0] == 1:
                maxflow += edge[2]
            _edges.write("%s;%s;%s;%s;%s;%s;%s;%s\n"
                         % (str(edge[0]), str(edge[1]), str(edge[2]),
                            str(edge[3]), str(edge[4]), str(edge[5]),
                            str(edge[6]), str(edge[7])))
        print("Maxflow = %d" % (maxflow))


if __name__ == '__main__':
    args = parser.parse_args()

    input_file = args.file
    param_file = args.params

    params = get_params(param_file)

    # from partitions, so make K = 1
    leftover = None
    if args.leftover is not None:
        params["K"] = 1
        leftover = args.leftover

    output_folder = os.path.abspath(os.path.join(input_file, os.pardir))

    matrix = create_matrix_from_input(input_file, params, leftover)

    graph = construct_the_graph(matrix, params)

    if verify_graph(graph, version=2):
        print("sample %s is verified, outputing graph..." % input_file)
        create_output(graph, version=2, output=output_folder)
    else:
        print("there is a problem with sample %s ..." % input_file)
