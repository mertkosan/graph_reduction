import os


def get_params(path):
    params = {
        "B": None,
        "N": None,
        "k": None,
        "K": None
    }
    with open(file=path, mode='r') as _params:
        line_params = _params.readline().strip().split(";")
        params["B"] = int(line_params[0])
        params["N"] = int(line_params[1])
        params["k"] = int(line_params[2])
        params["K"] = int(line_params[3])
        params["b"] = int(line_params[4])
    return params


def create_matrix_from_input(input_file):
    matrix = []
    with open(file=input_file, mode='r') as _file:
        for line in _file:
            matrix.append([(int(i.split(",")[0].strip()), int(i.split(",")[1].strip())) for i in line.split(";")])
    return matrix


def construct_the_graph_v1(matrix, params):
    """
    every node has 3 nodes which are supply, bike slot and demand nodes.
    """
    id = 5
    graph = {
        "source_node": (1, 0),
        "sink_node": (2, 0),
        "source_2_node": (3, -params["B"]),
        "sink_2_node": (4, params["B"]),
        "nodes": [],
        "edges": []
    }

    # create nodes
    for i in range(params["k"]*params["K"]):
        for j in range(params["N"]):
            graph["nodes"].append((id, matrix[i][j][0]))
            id += 1
            graph["nodes"].append((id, matrix[i][j][1]-matrix[i][j][0]))
            id += 1
            graph["nodes"].append((id, -matrix[i][j][1]))
            id += 1

    # create edges
    graph["edges"].append((graph["source_node"][0], graph["source_2_node"][0], abs(graph["source_2_node"][1]), 0))
    graph["edges"].append((graph["sink_2_node"][0], graph["sink_node"][0], abs(graph["sink_2_node"][1]), 0))

    # from main source and to main sink
    for k in range(len(graph["nodes"])):
        if graph["nodes"][k][1] < 0:
            graph["edges"].append((graph["source_node"][0], graph["nodes"][k][0], abs(graph["nodes"][k][1]), 0))
        elif graph["nodes"][k][1] > 0:
            graph["edges"].append((graph["nodes"][k][0], graph["sink_node"][0], abs(graph["nodes"][k][1]), 0))

    # from source2 to first supply nodes
    for k in range(0, 3*params["N"], 3):
        graph["edges"].append((graph["source_2_node"][0], graph["nodes"][k][0], graph["nodes"][k][1], 0))

    # from last demand nodes to sink2
    for k in range(len(graph["nodes"])-1, len(graph["nodes"])-1-3*params["N"], -3):
        graph["edges"].append((graph["nodes"][k][0], graph["sink_2_node"][0], abs(graph["nodes"][k][1]), 0))

    # from last bike slot nodes to sink2
    for k in range(len(graph["nodes"])-2, len(graph["nodes"])-2-3*params["N"], -3):
        graph["edges"].append((graph["nodes"][k][0], graph["sink_2_node"][0], -1, 0))

    # leftover bikes edges
    for k in range(1, len(graph["nodes"])-3*params["N"], 3*params["N"]):
        for i in range(params["N"]):
            graph["edges"].append((graph["nodes"][k+i*3][0], graph["nodes"][k+3*params["N"]+i*3][0], -1, 0))

    # transport bikes edges
    times = [int((time*params["k"])/2+1) for time in range(2*params["K"])]
    for time in times:
        nodes = [graph["nodes"][1 + (time-1) * 3 * params["N"] + i * 3][0] for i in range(params["N"])]
        for i in nodes:
            for j in nodes:
                if i != j:
                    graph["edges"].append((i, j, -1, params["b"]))

    return graph


def construct_the_graph_v2(matrix, params):
    """
    every node has 1 node which is only bike slot after supply and demand nodes are removed.
    """
    id = 4
    graph = {
        "source_node": (1, 0),
        "sink_node": (2, 0),
        "sink_2_node": (3, 0),
        "nodes": [],
        "edges": []
    }
    # set sink2_node demand
    first_layer_supply = sum([pair[0] for pair in matrix[0]])
    last_layer_demand = sum([pair[1] for pair in matrix[-1]])

    if last_layer_demand > first_layer_supply:
        print("ERROR, last_layer_demand cannot be bigger than first_layer_supply")

    graph["sink_2_node"] = (3, first_layer_supply-last_layer_demand)

    # central nodes
    for i in range(params["k"]*params["K"]):
        for j in range(params["N"]):
            graph["nodes"].append((id, matrix[i][j][1]-matrix[i][j][0]))
            id += 1

    # create edges
    graph["edges"].append((graph["sink_2_node"][0], graph["sink_node"][0], abs(graph["sink_2_node"][1]), 0))

    # from main source and to main sink
    for k in range(len(graph["nodes"])):
        if graph["nodes"][k][1] < 0:
            graph["edges"].append((graph["source_node"][0], graph["nodes"][k][0], abs(graph["nodes"][k][1]), 0))
        elif graph["nodes"][k][1] > 0:
            graph["edges"].append((graph["nodes"][k][0], graph["sink_node"][0], abs(graph["nodes"][k][1]), 0))

    # from last bike slot nodes to sink2
    for k in range(1, params["N"]+1):
        graph["edges"].append((graph["nodes"][-k][0], graph["sink_2_node"][0], -1, 0))

    # leftover bikes edges
    for k in range(0, len(graph["nodes"])-params["N"]-1, params["N"]):
        for i in range(params["N"]):
            graph["edges"].append((graph["nodes"][k+i][0], graph["nodes"][k+params["N"]+i][0], -1, 0))

    # transport bikes edges
    times = [int((time*params["k"])/2+1) for time in range(2*params["K"])]
    for time in times:
        nodes = [graph["nodes"][(time-1)*params["N"]+i][0] for i in range(params["N"])]
        for i in nodes:
            for j in nodes:
                if i != j:
                    graph["edges"].append((i, j, -1, params["b"]))

    return graph


def verify_graph(graph, version):
    total_demand = 0
    for node in graph["nodes"]:
        total_demand += node[1]
    total_demand += graph["sink_2_node"][1]
    if version == 1:
        total_demand += graph["source_2_node"][1]
    if total_demand != 0:
        return False
    else:
        return True


def create_output(graph, version, output):
    """
    take graph as an input, create a text file which has a name: output_name
    """
    if not os.path.exists(output):
        os.makedirs(output)

    with open(output + "/nodes_v" + str(version) + ".txt", "w") as _nodes:
        _nodes.write("%d;%d\n" % (graph["source_node"][0], graph["source_node"][1]))
        _nodes.write("%d;%d\n" % (graph["sink_node"][0], graph["sink_node"][1]))
        if version == 1:
            _nodes.write("%d;%d\n" % (graph["source_2_node"][0], graph["source_2_node"][1]))
        _nodes.write("%d;%d\n" % (graph["sink_2_node"][0], graph["sink_2_node"][1]))
        for node in graph["nodes"]:
            _nodes.write("%d;%d\n" % (node[0], node[1]))

    with open(output + "/edges_v" + str(version) + ".txt", "w") as _edges:
        for edge in graph["edges"]:
            _edges.write("%d;%d;%d;%d\n" % (edge[0], edge[1], edge[2], edge[3]))


if __name__ == '__main__':
    sample_count = 6
    start_sample = 0
    end_sample = sample_count
    for input_no in range(start_sample, end_sample+1, 1):
        output_no = input_no

        input_folder = "Inputs/" + str(input_no)
        output_folder = "Outputs/" + str(output_no)

        params = get_params(input_folder + "/params.txt")
        matrix = create_matrix_from_input(input_folder + "/inputs.txt")

        graph1 = construct_the_graph_v1(matrix, params)
        graph2 = construct_the_graph_v2(matrix, params)

        if verify_graph(graph1, version=1):
            print("sample %d is verified, outputing graph..." % input_no)
            create_output(graph1, version=1, output=output_folder)
        else:
            print("there is a problem with sample %d ..." % input_no)

        if verify_graph(graph2, version=2):
            print("sample %d is verified, outputing graph..." % input_no)
            create_output(graph2, version=2, output=output_folder)
        else:
            print("there is a problem with sample %d ..." % input_no)
